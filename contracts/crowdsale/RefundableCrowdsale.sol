pragma solidity ^0.5.2;

import "../../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./escrow/RefundEscrow.sol";
import "./TimedCrowdsale.sol";

contract RefundableCrowdsale is TimedCrowdsale {
    using SafeMath for uint256;

    // refund escrows used to hold funds while crowdsale is running
    RefundEscrow private _escrow1;
    RefundEscrow private _escrow2;
    RefundEscrow private _escrow3;
    RefundEscrow private _escrow4;

    address[4] private _escrowList;

    constructor () public {
        _escrow1 = new RefundEscrow(wallet());
        _escrow2 = new RefundEscrow(wallet());
        _escrow3 = new RefundEscrow(wallet());
        _escrow4 = new RefundEscrow(wallet());
        _escrowList = [address(_escrow1), address(_escrow2), address(_escrow3), address(_escrow4)];
    }

    function _claimFullRefund(address payable refundee) internal {
        // Empty escrow accounts backwards

        if (_escrow4.state() == RefundEscrow.State.Active) {
            _escrow4.withdraw(refundee);
        }
        if (_escrow3.state() == RefundEscrow.State.Active) {
            _escrow3.withdraw(refundee);
        }
        if (_escrow2.state() == RefundEscrow.State.Active) {
            _escrow2.withdraw(refundee);
        }
        if (_escrow1.state() == RefundEscrow.State.Active) {
            _escrow1.withdraw(refundee);
        }
        // TODO: Why is this code illegal. Error: Invalid opcode
        // for (uint8 i = 3; i>=0; i = i-1){
        //     RefundEscrow escrow = RefundEscrow(_escrowList[i]);
        //     if (escrow.state() == RefundEscrow.State.Active) {
        //         escrow.withdraw(refundee);
        //     }
        // }
    }

    function _claimPartialRefund(address payable refundee, uint256 refundAmount) internal {
        // Empty escrow accounts backwards
        for ( uint8 i = 3; i>=0; i--){
            if (refundAmount > 0) {
                refundAmount = _partialRefund(address(_escrowList[i]), refundee, refundAmount);
            } else {
                return;
            }
        }
    }

    function _partialRefund(address escrowAddress, address payable refundee, uint256 refundAmount) internal returns (uint256) {
        RefundEscrow escrow = RefundEscrow(escrowAddress);

        if (escrow.state() == RefundEscrow.State.Closed) {
            return 0;
        }

        uint256 balance = escrow.depositsOf(refundee);

        if (refundAmount >= balance) {
            escrow.withdraw(refundee);
            return refundAmount - balance;
        } else {
            escrow.withdrawPartial(refundee, refundAmount);
            return 0;
        }
    }

    /**
     * @dev Overrides Crowdsale fund forwarding, sending funds to escrow.
     */
    function _forwardFunds() internal {
        uint256 _quarterOfPayment = msg.value.div(4);
        uint256 _modulo = msg.value.mod(4);
        uint256 _restOfPayment = (_modulo == 0) ? _quarterOfPayment : _modulo;

        _escrow1.deposit.value(_quarterOfPayment)(msg.sender);
        _escrow2.deposit.value(_quarterOfPayment)(msg.sender);
        _escrow3.deposit.value(_quarterOfPayment)(msg.sender);
        _escrow4.deposit.value(_restOfPayment)(msg.sender);
    }

    function getEscrowBalance(address _addr) public view returns (uint256) {
        uint256 result = 0;
        result = (_escrow1.state() == RefundEscrow.State.Closed) ? result : result += _escrow1.depositsOf(_addr);
        result = (_escrow2.state() == RefundEscrow.State.Closed) ? result : result += _escrow2.depositsOf(_addr);
        result = (_escrow3.state() == RefundEscrow.State.Closed) ? result : result += _escrow3.depositsOf(_addr);
        result = (_escrow4.state() == RefundEscrow.State.Closed) ? result : result += _escrow4.depositsOf(_addr);
        return result;
    }

    function _beneficiaryWithdrawFromClosedEscrows() internal {
        for (uint8 i = 0; i<4; i++) {
            RefundEscrow escrow = RefundEscrow(_escrowList[i]);
            if (isPeriodClosed(TimedCrowdsale.Milestones(i))) {
                if (escrow.state() == RefundEscrow.State.Closed) {
                    escrow.beneficiaryWithdraw();
                }
            }
        }
    }

    function _checkTimeAndCloseEscrowsOverdue() internal {
        for (uint8 i = 0; i<4; i++) {
            RefundEscrow escrow = RefundEscrow(_escrowList[i]);
            if (isPeriodClosed(TimedCrowdsale.Milestones(i))) {
                if (escrow.state() == RefundEscrow.State.Active) {
                    escrow.close();
                }
            }
        }
    }

    modifier requiresMoneyOnEscrow() {
        require(getEscrowBalance(msg.sender) > 0, "ERR3");
        _;
    }
}
