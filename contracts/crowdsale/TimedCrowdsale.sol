pragma solidity ^0.5.2;

import "../../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../../node_modules/openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
// import "./CardCrowdsale.sol";

/**
 * @title TimedCrowdsale
 * @dev Crowdsale accepting contributions only within a time frame.
 */
contract TimedCrowdsale is Crowdsale {
    using SafeMath for uint256;

    uint256 private _openingTime;
    uint256 private _closingTime;

    uint256 private _firstPeriod;
    uint256 private _secondPeriod;
    uint256 private _thirdPeriod;
    uint256 private _fourthPeriod;

    enum Milestones { FirstPeriod, SecondPeriod, ThirdPeriod, FourthPeriod }

    /**
     * Event for crowdsale extending
     * @param newClosingTime new closing time
     * @param prevClosingTime old closing time
     */
    event TimedCrowdsaleExtended(uint256 prevClosingTime, uint256 newClosingTime);

    /**
     * @dev Reverts if not in crowdsale time range.
     */
    modifier onlyWhileOpen {
        require(isOpen(), "ERR4");
        _;
    }


    constructor (
        uint256 openingTime,
        uint256 closingTime,
        uint256 firstPeriod,
        uint256 secondPeriod,
        uint256 thirdPeriod,
        uint256 fourthPeriod )
        public {
        // solhint-disable-next-line not-rely-on-time
        require(openingTime >= block.timestamp, "ERR11");
        require(closingTime > openingTime, "ERR12");

        require(firstPeriod >= closingTime, "ERR13");
        require(secondPeriod > firstPeriod, "ERR14");
        require(thirdPeriod > secondPeriod, "ERR15");
        require(fourthPeriod > thirdPeriod, "ERR16");

        // sale start, stop
        _openingTime = openingTime;
        _closingTime = closingTime;

        // milestone end times
        _firstPeriod = firstPeriod;
        _secondPeriod = secondPeriod;
        _thirdPeriod = thirdPeriod;
        _fourthPeriod = fourthPeriod;
    }

    /**
     * @return the crowdsale opening time.
     */
    function openingTime() public view returns (uint256) {
        return _openingTime;
    }

    /**
     * @return the crowdsale closing time.
     */
    function closingTime() public view returns (uint256) {
        return _closingTime;
    }

    /**
     * @return true if the crowdsale is open, false otherwise.
     */
    function isOpen() public view returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return block.timestamp >= _openingTime && block.timestamp <= _closingTime;
    }

    /**
     * @dev Checks whether the period in which the crowdsale is open has already elapsed.
     * @return Whether crowdsale period has elapsed
     */
    function hasClosed() public view returns (bool) {
        // solhint-disable-next-line not-rely-on-time
        return block.timestamp > _closingTime;
    }

    /**
     * @dev Extend parent behavior requiring to be within contributing period.
     * @param beneficiary Token purchaser
     * @param weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal onlyWhileOpen view {
        super._preValidatePurchase(beneficiary, weiAmount);
    }

    /**
     * @dev Extend crowdsale.
     * @param newClosingTime Crowdsale closing time
     */
    function _extendTime(uint256 newClosingTime) internal {
        require(!hasClosed(), "ERR5");
        require(newClosingTime > _closingTime, "ERR6");

        emit TimedCrowdsaleExtended(_closingTime, newClosingTime);
        _closingTime = newClosingTime;
    }

    function isPeriodClosed(Milestones period) public view returns (bool) {
        require(uint(period) >= 0 && uint(period) <= 3, "ERR07");

        if (period == Milestones.FirstPeriod) {
            return block.timestamp >= _firstPeriod;
        }
        else if (period == Milestones.SecondPeriod) {
            return block.timestamp >= _secondPeriod;
        }
        else if (period == Milestones.ThirdPeriod) {
            return block.timestamp >= _thirdPeriod;
        }
        else if (period == Milestones.FourthPeriod) {
            return block.timestamp >= _fourthPeriod;
        }
    }
}