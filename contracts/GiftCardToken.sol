pragma solidity >=0.4.21 <0.6.0;

import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol';
import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol';
import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol';
import '../node_modules/openzeppelin-solidity/contracts/token/ERC20/IERC20.sol';
import '../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol';
import './crowdsale/RefundableCrowdsale.sol';
import "./crowdsale/TimedCrowdsale.sol";

contract GiftCardToken is ERC20, ERC20Detailed, RefundableCrowdsale, Ownable {
  using SafeMath for uint256;

    constructor (uint256 start, uint256 stop, uint256 first, uint256 second, uint256 third, uint256 fourth) public
        ERC20Detailed("GiftCards", "GCT", 18)
        TimedCrowdsale(start, stop, first, second, third, fourth)
        Crowdsale(1, msg.sender, IERC20(address(this))) {
    }

    function refund() public requiresMoneyOnEscrow {
      uint256 escrowBalance = getEscrowBalance(msg.sender);
      uint256 cardBalance = balanceOf(msg.sender);
      if (cardBalance < escrowBalance) {
        _burn(msg.sender, cardBalance);
        _claimPartialRefund(msg.sender, cardBalance);
      }
      else {
        uint256 delta = cardBalance.sub(escrowBalance);
        uint256 amountToBurn = cardBalance.sub(delta);
        _burn(msg.sender, amountToBurn);
        _claimFullRefund(msg.sender);
      }
    }

    function triggerMilestone() public onlyOwner {
      require(hasClosed(), "ERR21");
      require(isPeriodClosed(TimedCrowdsale.Milestones.FirstPeriod), "ERR22");
      _checkTimeAndCloseEscrowsOverdue();
      _beneficiaryWithdrawFromClosedEscrows();
    }

    function _deliverTokens(address beneficiary, uint256 tokenAmount) internal {
        require(safeMint(beneficiary, tokenAmount), "MintedCrowdsale: minting failed");
    }

    function safeMint(address to, uint256 value) internal returns (bool) {
        _mint(to, value);
        return true;
    }





}