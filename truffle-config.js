const HDWalletProvider = require("truffle-hdwallet-provider")
const infura_apikey = "TIkfho43TLfovnkTJhiC"
const shared_mnemonic =
  "glare modify fine lazy practice flee equip foam message insect camp approve"
const bc_mnemonic =
  "earth soldier purse ritual prize bird scrap repair basket shallow garden day"

module.exports = {
  migrations_directory: "./migrations",
  networks: {
    development: {  
      host: "localhost",
      port: 8545,
      network_id: "*", // Match any network id // 19071 / *
      gas: 8000000,
      gasPrice: 2000000000
    },
    bcnet: {
      provider: function () {
        return new HDWalletProvider(bc_mnemonic, "http://159.203.112.196:8540")
      },
      network_id: 558,
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider(
          shared_mnemonic,
          "https://rinkeby.infura.io/" + infura_apikey
        )
      },
      network_id: 4,
      gas: 7237638,
      gasPrice: 5000000000
    },
    ropsten: {
      provider: function () {
        return new HDWalletProvider(
          shared_mnemonic,
          "https://ropsten.infura.io/aJzbES9Dohx46EYrE4vg"
        )
      },
      network_id: 3
    },
    kovan: {
      provider: function () {
        return new HDWalletProvider(
          shared_mnemonic,
          "https://kovan.infura.io/aJzbES9Dohx46EYrE4vg"
        )
      },
      gas: 7900000,
      gasPrice: 100000000000 * 2,
      network_id: 42
    },
    gan: {
      network_id: 5777,
      gas: 9594037,
      port: 8545,
      gasPrice: 20000000000,
      confirmations: 2
    }
  },
  compilers: {
    solc: {
      version: "0.5.2",
      // settings: {
      //  optimizer: {
      //    enabled: false,
      //    runs: 200
      //  },
      //  evmVersion: "byzantium"
      // }

    }
  }
}
