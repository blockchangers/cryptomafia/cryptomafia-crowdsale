const GiftCardToken = artifacts.require("GiftCardToken");

module.exports = function(deployer) {
  let timeNow = parseInt(new Date().getTime() / 1000)

  // deployer.deploy(
  //   GiftCardToken, 
  //   1557200000, 
  //   1557300000, 
  //   1557400000, 
  //   1557500000, 
  //   1557600000, 
  //   1557700000
  // );
  deployer.deploy(
    GiftCardToken, 
    timeNow+10000,
    timeNow+20000,
    timeNow+30000,
    timeNow+40000,
    timeNow+50000,
    timeNow+60000,
  );
};
