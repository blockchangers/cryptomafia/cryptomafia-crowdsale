const { BN, constants, expectEvent, shouldFail } = require('openzeppelin-test-helpers');
const { expect } = require('chai');
const { advanceTimeAndBlock } = require('./utils/timeTravel');
var GiftCardToken = artifacts.require("./GiftCardToken.sol");

// Always restart testrpc/ganache before launching new tests.

contract('GiftCardToken', async function(accounts) {
    const SECONDS_IN_A_DAY = 86400
    var originalBlock

    const owner = accounts[0]
    const user2 = accounts[1]
    const user3 = accounts[2]
    const user4 = accounts[3]
    const user5 = accounts[4]
    const user6 = accounts[5]
    const user7 = accounts[6]
    const user8 = accounts[7]
    const user9 = accounts[8]
    const user10 = accounts[9]

    var openingTime 
    var closingTime
    var firstPeriod 
    var secondPeriod
    var thirdPeriod 
    var fourthPeriod
    var _token
    var newBlock
    var timeDiff

    before("SETTING UP CONSTANTS AND CREATING A NEW GIFT CARD SALE", async () => {
        originalBlock   = await web3.eth.getBlock('latest')
        openingTime     = parseInt(originalBlock.timestamp) + SECONDS_IN_A_DAY
        closingTime     = openingTime + (SECONDS_IN_A_DAY * 3)
        firstPeriod     = closingTime + SECONDS_IN_A_DAY
        secondPeriod    = firstPeriod + SECONDS_IN_A_DAY
        thirdPeriod     = secondPeriod + SECONDS_IN_A_DAY
        fourthPeriod    = thirdPeriod + SECONDS_IN_A_DAY
        _token          = await GiftCardToken.new(openingTime, closingTime, firstPeriod, secondPeriod, thirdPeriod, fourthPeriod)
    })

    
    it("TIME ADVANCEMENT TEST", async () => {
        const advancement   = 1000
        newBlock            = await advanceTimeAndBlock(advancement)
        timeDiff            = newBlock.timestamp - originalBlock.timestamp
        console.log('originalBlock.timestamp    |', originalBlock.timestamp)
        console.log('newBlock.timestamp         |', newBlock.timestamp)
        console.log('timeDiff                   |', timeDiff)
        assert.isTrue(timeDiff > advancement)
    })

    //////////////////////
    // STAGE: 
    // BEFORE OPENING SALE
    
    it("USER SHOULD FAIL TO BUY TOKEN WHEN THE CROWDSALE HAS NOT STARTED", async () => {
        // ARRANGE 
        let user2BalanceBefore    = await _token.balanceOf.call(user2)
        
        // ACT
        let tx                    = _token.buyTokens(user2, {from: user2, value: web3.utils.toWei('5', 'ether')})
        
        // ASSERT
        shouldFail.reverting(tx) // SHOULDFAIL WILL AWAIT THE TX PROMISE
        let user2BalanceAfter     = await _token.balanceOf.call(user2)
        console.log('User2 Card bal      | Before   |', user2BalanceBefore.toString())
        console.log('User2 Card bal      | After    |', user2BalanceAfter.toString())
        assert.isTrue(user2BalanceAfter.eq(user2BalanceBefore), "User2 balance after should be greater than balance before.")
    })

    //////////////////////
    // STAGE: 
    // CROWDSALE OPEN

    it("TIME ADVANCE: CROWDSALE OPEN", async () => {
        await intoTheFuture(openingTime)
    })

    it("USER SHOULD BE ABLE TO BUY TOKENS WHEN THE CROWDSALE HAS STARTED", async () => {

        // ARRANGE
        let user3BalanceBefore    = await _token.balanceOf.call(user3)
        
        // ACTION
        let buyTx                 = await _token.buyTokens(user3, {from: user3, value: web3.utils.toWei('1', 'ether')})
        
        // ASSERT
        let user3BalanceAfter     = await _token.balanceOf.call(user3)
        console.log('User3 Card bal      | Before', user3BalanceBefore.toString())
        console.log('User3 Card bal      | After', user3BalanceAfter.toString())
        assert.isTrue(user3BalanceAfter.gt(user3BalanceBefore), "User3 balance after should be greater than balance before.")
    })

    it("USER SHOULD BE ABLE TO REFUND THEIR TOKENS BEFORE CROWDSALE IS CLOSED (OR AT ANY TIME) (REGARDING ALL TOKENS NOT CLAIMED)", async () => {
        // ARRANGE 
        let tokenBalanceBefore        = await _token.balanceOf.call(user3)
        let etherBalanceBefore        = new BN(await web3.eth.getBalance(user3))
        let getEscrowBalanceBefore    = await _token.getEscrowBalance.call(user3)
        
        // ACTION
        let refundTx                  = await _token.refund({from: user3})
        
        // ASSERT
        let tokenBalanceAfter         = await _token.balanceOf.call(user3)
        let etherBalanceAfter         = new BN(await web3.eth.getBalance(user3))
        let getEscrowBalanceAfter     = await _token.getEscrowBalance.call(user3)
        console.log('User3 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('User3 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('User3 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('User3 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', getEscrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', getEscrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "User3 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "User3 ether balance should be increased after received refund.")
        assert.isTrue(getEscrowBalanceBefore.gt(getEscrowBalanceAfter), "Escrow balance should have decreased after refund process.")
    })
    
    it("MANY USERS SHOULD BE ABLE TO BUY TOKENS WHEN CROWDSALE HAS STARTED", async () => {
        // ARRANGE 
        let user2BalanceBefore    = await _token.balanceOf.call(user2)
        let user3BalanceBefore    = await _token.balanceOf.call(user3)
        let user4BalanceBefore    = await _token.balanceOf.call(user4)
        
        // ACT 
        let user2tx               = await _token.buyTokens(user2, {from: user2, value: web3.utils.toWei('2', 'ether')})
        let user3tx               = await _token.buyTokens(user3, {from: user3, value: web3.utils.toWei('3', 'ether')})
        let user4tx               = await _token.buyTokens(user4, {from: user4, value: web3.utils.toWei('4', 'ether')})
        // user5 does not buy any cards
        let user6tx               = await _token.buyTokens(user6, {from: user6, value: web3.utils.toWei('5', 'ether')})
        let user7tx               = await _token.buyTokens(user7, {from: user7, value: web3.utils.toWei('6', 'ether')})
        let user8tx               = await _token.buyTokens(user8, {from: user8, value: web3.utils.toWei('7', 'ether')})
        let user9tx               = await _token.buyTokens(user9, {from: user9, value: web3.utils.toWei('10', 'ether')})
        
        // ASSERT
        let user2BalanceAfter     = await _token.balanceOf.call(user2)
        let user3BalanceAfter     = await _token.balanceOf.call(user3)
        let user4BalanceAfter     = await _token.balanceOf.call(user4)
        console.log('User2 Card bal      | Before   |', user2BalanceBefore.toString())
        console.log('User3 Card bal      | Before   |', user3BalanceBefore.toString())
        console.log('User4 Card bal      | Before   |', user4BalanceBefore.toString())
        console.log('User2 Card bal      | After    |', user2BalanceAfter.toString())
        console.log('User3 Card bal      | After    |', user3BalanceAfter.toString())
        console.log('User4 Card bal      | After    |', user4BalanceAfter.toString())
        assert.isTrue(user2BalanceAfter.gt(user2BalanceBefore), "User2 balance after should be greater than balance before.")
        assert.isTrue(user3BalanceAfter.gt(user3BalanceBefore), "User3 balance after should be greater than balance before.")
        assert.isTrue(user4BalanceAfter.gt(user4BalanceBefore), "User4 balance after should be greater than balance before.")
    })
    
    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS BEFORE CROWDSALE HAS CLOSED", async () => {
        // ARRANGE 
        let ownerBalanceBefore    = await _token.balanceOf.call(owner)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(owner))
        
        // ACT
        let tx                    = _token.triggerMilestone({from: owner})
        
        // ASSERT
        shouldFail.reverting(tx) // SHOULDFAIL WILL AWAIT THE TX PROMISE
        let ownerBalanceAfter     = await _token.balanceOf.call(owner)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', ownerBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', ownerBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(ownerBalanceAfter.eq(ownerBalanceBefore), "owner balance after should be greater than balance before.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "owner balance after should not be greater than balance before.")
    })

    //////////////////////
    // STAGE: 
    // CROWDSALE CLOSED

    it("TIME ADVANCE: CLOSING TIME", async () => {
        await intoTheFuture(closingTime)
    })

    it("USER SHOULD FAIL TO BUY TOKENS WHEN THE CROWDSALE HAS BEEN CLOSED", async () => {
        // ARRANGE 
        let user5BalanceBefore    = await _token.balanceOf.call(user5)
        
        // ACT
        let tx                    = _token.buyTokens(user5, {from: user5, value: web3.utils.toWei('12', 'ether')})
        
        // ASSERT 
        shouldFail.reverting(tx)
        let user5BalanceAfter     = await _token.balanceOf.call(user5)
        console.log('User5 Card bal      | Before', user5BalanceBefore.toString())
        console.log('User5 Card bal      | After', user5BalanceAfter.toString())
        assert.isTrue(user5BalanceAfter.eq(user5BalanceBefore), "User5 balance after should be greater than balance before.")
    })

    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS BEFORE ANY DEVELOPMENT STAGES HAVE BEEN FINALIZED", async () => {
        // ARRANGE 
        let ownerBalanceBefore    = await _token.balanceOf.call(owner)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(owner))
        
        // ACT
        let tx                    = _token.triggerMilestone({from: owner})
        
        // ASSERT
        shouldFail.reverting(tx) 
        let ownerBalanceAfter     = await _token.balanceOf.call(owner)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', ownerBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', ownerBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(ownerBalanceAfter.eq(ownerBalanceBefore), "owner balance after should be greater than balance before.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "owner balance after should not be greater than balance before.")
    })

    //////////////////////
    // STAGE: 
    // PERIOD 1

    it("TIME ADVANCE: PERIOD 1", async () => {
        await intoTheFuture(firstPeriod)
    })

    it("PERIOD 1: NON-OWNERS SHOULD NOT BE ABLE TO TRIGGER MILESTONES AND CLAIM FUNDS", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(user4)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(user4))
        
        // ACT 
        let tx                  = _token.triggerMilestone({from: user4})
        
        // ASSERT 
        shouldFail.reverting(tx)
        let cardBalanceAfter    = await _token.balanceOf.call(user4)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(user4))
        console.log('user4 Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('user4 ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('user4 Card bal      | After    |', cardBalanceAfter.toString())
        console.log('user4 ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of user 4 should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of user 4 should not be higher than it was.")
    })

    it("OWNERS SHOULD BE ABLE TO CLAIM FUNDS AFTER PERIOD 1", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceBefore = await _token.getEscrowBalance.call(user4)
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceAfter  = await _token.getEscrowBalance.call(user4)
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        console.log('Escrow balance      | Before   |', escrowBalanceBefore.toString())
        console.log('Escrow balance      | After    |', escrowBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.gt(etherBalanceBefore), "Ether balance of owner should be higher than it was.")
        assert.isTrue(escrowBalanceAfter.lt(escrowBalanceBefore), "Escrow balance of user 3 should be less than it was, because owner has withdrawn monies.")
    })

    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS MORE THAN ONE TIME PER STAGE", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of owner should not be higher than it was.")
        
    })

    it("USERS SHOULD BE ABLE TO REFUND ONLY NON-CLAIMED FUNDS AT PERIOD 1", async () => {
        // ARRANGE 
        let tokenBalanceBefore        = await _token.balanceOf.call(user8)
        let etherBalanceBefore        = new BN(await web3.eth.getBalance(user8))
        let getEscrowBalanceBefore    = await _token.getEscrowBalance.call(user8)
        
        // ACTION
        let refundTx                  = await _token.refund({from: user8})
        
        // ASSERT
        let tokenBalanceAfter         = await _token.balanceOf.call(user8)
        let etherBalanceAfter         = new BN(await web3.eth.getBalance(user8))
        let getEscrowBalanceAfter     = await _token.getEscrowBalance.call(user8)
        console.log('user8 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user8 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user8 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user8 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', getEscrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', getEscrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "user8 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "user8 ether balance should be increased after received refund.")
        assert.isTrue(getEscrowBalanceBefore.gt(getEscrowBalanceAfter), "Escrow balance should have decreased after refund process.")


    })

    it("USER SHOULD SPEND SOME GIFT CARDS BEFORE CLAIMING A REFUND", async () => {
        // ARRANGE 

        let tokenBalanceBefore        = await _token.balanceOf.call(user9) // 10
        let etherBalanceBefore        = new BN(await web3.eth.getBalance(user9)) // 90 
        let getEscrowBalanceBefore    = await _token.getEscrowBalance.call(user9) // 10 -> 7.5
        
        // ACTION
        let spendTx                   = await _token.transfer(user10, web3.utils.toWei('5', 'ether'), {from: user9}) // token bal: 10 - 5 = 5
        let refundTx                  = await _token.refund({from: user9}) // 90 -> 94.99 
        
        // ASSERT
        let tokenBalanceAfter         = await _token.balanceOf.call(user9)
        let etherBalanceAfter         = new BN(await web3.eth.getBalance(user9))
        let getEscrowBalanceAfter     = await _token.getEscrowBalance.call(user9)
        console.log('user9 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user9 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user9 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user9 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', getEscrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', getEscrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "user9 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "user9 ether balance should be increased after received refund.")
        assert.isTrue(getEscrowBalanceBefore.gt(getEscrowBalanceAfter), "Escrow balance should have decreased after refund process.")


    })


    //////////////////////
    // STAGE: 
    // PERIOD 2

    it("TIME ADVANCE: PERIOD 2", async () => {
        await intoTheFuture(secondPeriod)
    })

    it("PERIOD 2: NON-OWNERS SHOULD NOT BE ABLE TO TRIGGER MILESTONES AND CLAIM FUNDS", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(user2)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(user2))
        
        // ACT 
        let tx                  = _token.triggerMilestone({from: user2})
        
        // ASSERT 
        shouldFail.reverting(tx)
        let cardBalanceAfter    = await _token.balanceOf.call(user2)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(user2))
        console.log('user2 Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('user2 ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('user2 Card bal      | After    |', cardBalanceAfter.toString())
        console.log('user2 ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of user2 should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of user2 should not be higher than it was.")
    })

    it("OWNERS SHOULD BE ABLE TO CLAIM FUNDS AFTER PERIOD 2", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceBefore = await _token.getEscrowBalance.call(user3)
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceAfter  = await _token.getEscrowBalance.call(user3)
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        console.log('Escrow balance      | Before   |', escrowBalanceBefore.toString())
        console.log('Escrow balance      | After    |', escrowBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.gt(etherBalanceBefore), "Ether balance of owner should be higher than it was.")
        assert.isTrue(escrowBalanceAfter.lt(escrowBalanceBefore), "Escrow balance of user3 should be less than it was, because owner has withdrawn monies.")
    })

    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS MORE THAN ONE TIME PER STAGE 2", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of owner should not be higher than it was.")
        
    })

    it("USERS SHOULD BE ABLE TO REFUND ONLY NON-CLAIMED FUNDS AT PERIOD 2", async () => {
        // ARRANGE 
        let tokenBalanceBefore    = await _token.balanceOf.call(user7)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(user7))
        let escrowBalanceBefore   = await _token.getEscrowBalance.call(user7)
        
        // ACTION
        let refundTx              = await _token.refund({from: user7})
        
        // ASSERT
        let tokenBalanceAfter     = await _token.balanceOf.call(user7)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(user7))
        let escrowBalanceAfter    = await _token.getEscrowBalance.call(user7)
        console.log('user7 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user7 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user7 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user7 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', escrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', escrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "user7 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "user7 ether balance should be increased after received refund.")
        assert.isTrue(escrowBalanceBefore.gt(escrowBalanceAfter), "Escrow balance should have decreased after refund process.")
    })

    //////////////////////
    // STAGE: 
    // PERIOD 3

    it("TIME ADVANCE: PERIOD 3", async () => {
        await intoTheFuture(thirdPeriod)
    })

    it("PERIOD 3: NON-OWNERS SHOULD NOT BE ABLE TO TRIGGER MILESTONES AND CLAIM FUNDS", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(user4)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(user4))
        
        // ACT 
        let tx                  = _token.triggerMilestone({from: user4})
        
        // ASSERT 
        shouldFail.reverting(tx)
        let cardBalanceAfter    = await _token.balanceOf.call(user4)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(user4))
        console.log('user4 Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('user4 ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('user4 Card bal      | After    |', cardBalanceAfter.toString())
        console.log('user4 ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of user4 should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of user4 should not be higher than it was.")
    })

    it("OWNERS SHOULD BE ABLE TO CLAIM FUNDS AFTER PERIOD 3", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceBefore = await _token.getEscrowBalance.call(user2)
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceAfter  = await _token.getEscrowBalance.call(user2)
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        console.log('Escrow balance      | Before   |', escrowBalanceBefore.toString())
        console.log('Escrow balance      | After    |', escrowBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.gt(etherBalanceBefore), "Ether balance of owner should be higher than it was.")
        assert.isTrue(escrowBalanceAfter.lt(escrowBalanceBefore), "Escrow balance of user2 should be less than it was, because owner has withdrawn monies.")
    })

    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS MORE THAN ONE TIME PER PERIOD 3", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of owner should not be higher than it was.")
        
    })

    it("USERS SHOULD BE ABLE TO REFUND ONLY NON-CLAIMED FUNDS AT PERIOD 3", async () => {
        // ARRANGE 
        let tokenBalanceBefore    = await _token.balanceOf.call(user6)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(user6))
        let escrowBalanceBefore   = await _token.getEscrowBalance.call(user6)
        
        // ACTION
        let refundTx              = await _token.refund({from: user6})
        
        // ASSERT
        let tokenBalanceAfter     = await _token.balanceOf.call(user6)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(user6))
        let escrowBalanceAfter    = await _token.getEscrowBalance.call(user6)
        console.log('user6 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user6 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user6 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user6 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', escrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', escrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "user6 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "user6 ether balance should be increased after received refund.")
        assert.isTrue(escrowBalanceBefore.gt(escrowBalanceAfter), "Escrow balance should have decreased after refund process.")
    })

    //////////////////////
    // STAGE: 
    // PERIOD 4
    
    it("TIME ADVANCE: PERIOD 4", async () => {
        await intoTheFuture(fourthPeriod)
    })

    it("USERS SHOULD BE ABLE TO CLAIM REFUNDS AT PERIOD 4 BEFORE CLAIMED BY OWNERS", async () => {
        // ARRANGE 
        let tokenBalanceBefore    = await _token.balanceOf.call(user2)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(user2))
        let escrowBalanceBefore   = await _token.getEscrowBalance.call(user2)
        
        // ACTION
        let refundTx              = await _token.refund({from: user2})
        
        // ASSERT
        let tokenBalanceAfter     = await _token.balanceOf.call(user2)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(user2))
        let escrowBalanceAfter    = await _token.getEscrowBalance.call(user2)
        console.log('user2 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user2 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user2 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user2 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', escrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', escrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.gt(tokenBalanceAfter), "user2 token balance should be less after refunding gift cards to ether.")
        assert.isTrue(etherBalanceBefore.lt(etherBalanceAfter), "user2 ether balance should be increased after received refund.")
        assert.isTrue(escrowBalanceBefore.gt(escrowBalanceAfter), "Escrow balance should have decreased after refund process.")
    })

    it("PERIOD 4: NON-OWNERS SHOULD NOT BE ABLE TO TRIGGER MILESTONES AND CLAIM FUNDS", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(user4)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(user4))
        
        // ACT 
        let tx                  = _token.triggerMilestone({from: user4})
        
        // ASSERT 
        shouldFail.reverting(tx)
        let cardBalanceAfter    = await _token.balanceOf.call(user4)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(user4))
        console.log('user4 Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('user4 ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('user4 Card bal      | After    |', cardBalanceAfter.toString())
        console.log('user4 ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of user4 should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of user4 should not be higher than it was.")
    })

    it("OWNERS SHOULD BE ABLE TO CLAIM FUNDS AFTER PERIOD 4", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceBefore = await _token.getEscrowBalance.call(user3)
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        let escrowBalanceAfter  = await _token.getEscrowBalance.call(user3)
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        console.log('Escrow balance      | Before   |', escrowBalanceBefore.toString())
        console.log('Escrow balance      | After    |', escrowBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.gt(etherBalanceBefore), "Ether balance of owner should be higher than it was.")
        assert.isTrue(escrowBalanceAfter.lt(escrowBalanceBefore), "Escrow balance of user3 should be less than it was, because owner has withdrawn monies.")
    })

    it("OWNERS SHOULD NOT BE ABLE TO CLAIM FUNDS MORE THAN ONE TIME PER PERIOD 4", async () => {
        // ARRANGE 
        let cardBalanceBefore   = await _token.balanceOf.call(owner)
        let etherBalanceBefore  = new BN(await web3.eth.getBalance(owner))
        
        // ACT 
        let tx                  = await _token.triggerMilestone({from: owner})
        
        // ASSERT 
        let cardBalanceAfter    = await _token.balanceOf.call(owner)
        let etherBalanceAfter   = new BN(await web3.eth.getBalance(owner))
        console.log('owner Card bal      | Before   |', cardBalanceBefore.toString())
        console.log('owner ether bal     | Before   |', etherBalanceBefore.toString())
        console.log('owner Card bal      | After    |', cardBalanceAfter.toString())
        console.log('owner ether bal     | After    |', etherBalanceAfter.toString())
        assert.isTrue(cardBalanceAfter.eq(cardBalanceBefore), "Card balance of owner should not be higher than it was.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "Ether balance of owner should not be higher than it was.")
        
    })

    it("USERS SHOULD NOT BE ABLE TO REFUND FUNDS AT PERIOD 4, WHEN THEY HAVE ALL BEEN CLAIMED", async () => {
        // ARRANGE 
        let tokenBalanceBefore    = await _token.balanceOf.call(user4)
        let etherBalanceBefore    = new BN(await web3.eth.getBalance(user4))
        let escrowBalanceBefore   = await _token.getEscrowBalance.call(user4)
        
        // ACTION
        let refundTx              = _token.refund({from: user4})
        
        // ASSERT
        shouldFail.reverting(refundTx)
        let tokenBalanceAfter     = await _token.balanceOf.call(user4)
        let etherBalanceAfter     = new BN(await web3.eth.getBalance(user4))
        let escrowBalanceAfter    = await _token.getEscrowBalance.call(user4)
        console.log('user4 Card bal       | Before  |', tokenBalanceBefore.toString())
        console.log('user4 Card bal       | After   |', tokenBalanceAfter.toString())
        console.log('user4 Ether bal      | Before  |', etherBalanceBefore.toString())
        console.log('user4 Ether bal      | After   |', etherBalanceAfter.toString())
        console.log('Escrow balance       | Before  |', escrowBalanceBefore.toString())
        console.log('Escrow balance       | After   |', escrowBalanceAfter.toString())
        assert.isTrue(tokenBalanceBefore.eq(tokenBalanceAfter), "user4 token balance should be equal after failed refunds.")
        assert.isTrue(etherBalanceAfter.lte(etherBalanceBefore), "user4 ether balance should be not increased after received refund.")
        assert.isTrue(escrowBalanceBefore.eq(escrowBalanceAfter), "Escrow balance should have decreased after refund process.")
    })

    async function intoTheFuture(targetStage) {
        let latestBlock = await web3.eth.getBlock('latest')
        let latestBlockTime = parseInt(latestBlock.timestamp)
        const advancement = (targetStage - latestBlockTime) + 10
        newBlock = await advanceTimeAndBlock(advancement)
    }


})